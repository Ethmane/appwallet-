
import React from 'react';
import { View,  StyleSheet, ActivityIndicator } from 'react-native';
import {APP_COLORS} from '../constants/Colors';

const Spinner =  ({size, color, loading}) => {
    
	return (
		<View style={styles.container}>
			<ActivityIndicator
				size={size || 'large'}
				color={color|| APP_COLORS.primaryAction }
				animating={loading} 
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#2c3e50',
	},
});

export {Spinner};
