import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {FadingView} from '../components';
import {APP_COLORS} from '../constants/Colors';

const ErrorModal = ({style, visible, children}) => {
	const { errorContainer, errorText } = styles;
	return (
		<FadingView visible={visible} style={[errorContainer, style]}>  
			<Text style={errorText} >
				{children}    
			</Text>
		</FadingView>
	);
};

const styles = StyleSheet.create({
	errorContainer: {
		backgroundColor: APP_COLORS.darkPrimary || 'gray',
		width: 300, 
		marginTop: 40,
		alignSelf: 'center',  
		paddingHorizontal: 10,
		paddingVertical: 5,
		borderRadius: 5,
		zIndex: 2
	},
	errorText: {
		fontSize: 14,
		color: APP_COLORS.primary || 'white',
		lineHeight: 22,
	},  
});

export {ErrorModal};
