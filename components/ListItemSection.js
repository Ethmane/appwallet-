import React from 'react';
import { View } from 'react-native';
import {APP_COLORS} from '../constants/Colors';


const ListItemSection = ({ children }) => {
	return (
		<View style={styles.containerStyle }>
			{children} 
		</View>
	);
};

const styles = {
	containerStyle: {
		flex: 1,
		borderColor: APP_COLORS.lines || 'gray',
		borderWidth: 1,
	}
};

export {ListItemSection};


