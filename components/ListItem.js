import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { Container, Header, Content, SwipeRow, Icon, Button, List, ListItem as ListItemNB, Left, Body, Right, Thumbnail} from 'native-base';
import Modal from 'react-native-modal';
import {APP_COLORS} from '../constants/Colors';


class ListItem extends React.Component{

    _onPress = () => {
        this.props.onPressItem(this.props.item);
      };

    render(){

        const {textFullName, textPreview , container,btnAnnuler,
            btnSupprimer, textbtn, btnModal,
            textheaderModal} = styles;

        const {children, last, onLongPress} = this.props;

        return (

            <View style= {container}>
            
                <TouchableOpacity 
                onPress={this._onPress}
                style={{paddingHorizontal: 8}}
                >
                    <View style={ styles.containerStyle }>
                        {children} 
                    </View>

                </TouchableOpacity>
            </View>
        );
   }
   
};

const styles = {
    container: {
        flex: 1,
        borderRadius : 1,
    },
    containerStyle: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: APP_COLORS.secondaryText || 'gray',
    },
    textbtn : {
        color : '#FFFFFF',
        fontSize : 16,
        fontWeight : 'bold',
    },
    btnAnnuler : {
        backgroundColor : '#8285d6',
        height: 50,
        width : 140,
        alignSelf : 'center',
        justifyContent : 'center',
        marginLeft : 20,
    },
    btnSupprimer: {
        backgroundColor :'red',
        height: 50,
        width : 140,
        alignSelf : 'center',
        justifyContent : 'center',
        marginRight : 20,
    },
    btnModal : {
        justifyContent : 'center', 
        alignSelf : 'center'
    }
};

export {ListItem};

