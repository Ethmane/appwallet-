import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../constants/Colors'; 

const LightButtonForm = ({ onPress, children, color, backgroundColor }) => {
	const { buttonStyle, textStyle, buttonDisabled, buttonEnabled} = styles;
  
	return (
		<TouchableOpacity onPress={onPress} 
			style={ [buttonStyle, {borderColor: color || 'gray', backgroundColor: backgroundColor || 'transparent' }] } >
			<Text style={ [textStyle, {color: color || 'gray'}] } >
				{children}
			</Text>
		</TouchableOpacity>
	);
};

const styles = {
	textStyle: {
		alignSelf: 'center',
		fontSize: 16 ,    
		fontWeight: '500',
		paddingTop: 10,
		paddingBottom: 10,
  
	},
	buttonStyle: {
		alignItems: 'center',
		justifyContent: 'center',
		flex: 1,
		width: '100%',
		height: 40,
		borderRadius: 5,
		borderWidth: 1,
    
	},

  
};

export {LightButtonForm};


