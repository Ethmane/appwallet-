
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';


const Button = ({onPress, children}) => {
	return (
		<TouchableOpacity onPress={ () => onPress()}>
			<View style={styles.container}>
				<Text  style={styles.text}>{children}</Text>
			</View>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#8285d6',
		borderRadius: 4,
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		color: 'white',
		fontSize: 16,
		paddingVertical: 6,
		paddingHorizontal: 9,
	},
});

export {Button};
