import React from 'react';
import { View, Text, TextInput } from 'react-native';
import { APP_COLORS } from '../constants/Colors';
import { TextInputMask } from 'react-native-masked-text';
import PropTypes from 'prop-types';

const InputForm = ({ active, type, onFocus, editable, label, value, onChangeText, placeholder, secureTextEntry, onBlur, symboleMonnaie }) => {
	const { symboleContainer, inputStyle, labelStyle, containerStyle, symboleStyle } = styles;
       
	return (
		<View 
			style={ (active) ? containerStyle : [containerStyle ,{borderWidth: 0.5}]} 
		>
			<Text 
				style={ (active) ? labelStyle : [labelStyle, {color: APP_COLORS.secondaryText || 'gray'}]} 
			>{label}</Text>
			<View style={symboleContainer} >
				{type === "only-numbers" ? (
					<TextInputMask 
					type={'only-numbers'}
					onFocus={onFocus}
					editable={editable}
					secureTextEntry={secureTextEntry}
					placeholder={placeholder}
					autoCorrect={false}
					style={inputStyle}
					value={value}
					onChangeText={onChangeText}
					onBlur={onBlur}
					autoCapitalize='none'
					underlineColorAndroid="rgba(0,0,0,0)"   
					maxLength={10}
					/>
					) : (
					<TextInput 
					onFocus={onFocus}
					editable={editable}
					secureTextEntry={secureTextEntry}
					placeholder={placeholder}
					autoCorrect={false}
					style={inputStyle}
					value={value}
					onChangeText={onChangeText}
					onBlur={onBlur}
					autoCapitalize='none'
					underlineColorAndroid="rgba(0,0,0,0)"   
					maxLength={10}
					/>
				)}
				<Text 
					style={symboleStyle} 
				>{symboleMonnaie}</Text>
			</View>
           
		</View>);
};

InputForm.propTypes = {
	active: PropTypes.bool,
	onFocus: PropTypes.func,
	placeholder: PropTypes.string,
	editable: PropTypes.bool,
	label: PropTypes.string,
	onChangeText: PropTypes.func,
	value: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number
	]),
	onBlur: PropTypes.func,
	symboleMonnaie: PropTypes.string,
	secureTextEntry: PropTypes.bool,
};

const styles = {
	containerStyle: { 
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center', 
		paddingHorizontal: '3%',
		borderRadius: 5,
		borderWidth: 1,
		borderColor: APP_COLORS.secondaryText  || 'gray',  
		alignSelf: 'stretch',
		backgroundColor: APP_COLORS.primary || 'white',
	},
	inputStyle: {
		color:  APP_COLORS.primaryText || 'black',
		fontSize: 16,
		lineHeight: 18,
		flex: 8,    
		fontWeight: '600',
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 15, 
		alignContent : 'flex-start',
	},
	labelStyle: {   
		fontSize: 16,
		flex: 8,
	},
	symboleStyle : {
		fontSize: 16,
		flex: 1,    
	},
	symboleContainer: {
		flex: 6,
		flexDirection: 'row',
		alignItems: 'center',
	},
};

export { InputForm };