import React from 'react';
import { View, Text } from 'react-native';
import {APP_COLORS} from '../constants/Colors';

export default TabBarLabel =  ({name, focused}) => {
    const textColor = (focused) ? APP_COLORS.primaryAction : APP_COLORS.darkPrimary;
	return (
			<Text style={{flex: 1, color: textColor, alignSelf: 'center'}}>{name}</Text>
	);
};

