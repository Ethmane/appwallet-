
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {APP_COLORS} from '../constants/Colors';

const Header = (props) => {
   
	return (
		<View style={styles.viewStyle}>
			<Text style={styles.text}>{props.headerTitle}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	text: {
		fontSize: 20, 
		color: APP_COLORS.primary,
	},
	viewStyle: {
		backgroundColor: APP_COLORS.darkPrimary || 'gray',
		justifyContent: 'center',
		alignItems: 'center',
		height: 80,
		paddingTop: 15,
		shadowColor: '#000',
		shadowOffset: {width: 0, height: 4},
		shadowOpacity: 0.2,
		elevation: 4,
		position: 'relative'
	}
});

export  {Header};
