import React from 'react';
import { Icon } from 'expo';
import {APP_COLORS} from '../constants/Colors';

export default class TabBarIcon extends React.Component {
	render() {
		return (
			<Icon.Ionicons
				name={this.props.name}
				size={26}
				style={{ marginBottom: -3 , marginTop: 10 }}
				color={this.props.focused ? APP_COLORS.primaryAction : APP_COLORS.darkPrimary}
			/>
		);
	}
}