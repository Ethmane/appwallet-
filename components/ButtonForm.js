import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../constants/Colors';

const ButtonForm = ({ onPress, children, disabled }) => {
	const { buttonStyle, textStyle, buttonDisabled, buttonEnabled} = styles;

	return (
		<TouchableOpacity onPress={onPress} 
			style={ (disabled) ? [buttonStyle,buttonDisabled ] : [buttonStyle,buttonEnabled ]} disabled={disabled}>
			<Text style={ (disabled) ? [textStyle,{color: APP_COLORS.secondaryText || 'gray'}] : [textStyle, {color: APP_COLORS.primary || 'white', borderColor: APP_COLORS.primary || 'white'}]} >
				{children}
			</Text>
		</TouchableOpacity>
	);
};

const styles = {
	textStyle: {
		alignSelf: 'center',
		fontSize: 16 ,    
		fontWeight: '600',
	},
	buttonStyle: {
		flex: 1,
		justifyContent: 'center',
		height: 40,
		borderRadius: 5,
		borderWidth: 1,
	},
	buttonDisabled: {
		backgroundColor: APP_COLORS.primary || 'white',
		borderColor: APP_COLORS.secondaryText || 'gray',
	},
	buttonEnabled: {
		backgroundColor: APP_COLORS.primaryAction || 'red',
		borderColor: APP_COLORS.primary || 'white',
	}
};
export {ButtonForm};


