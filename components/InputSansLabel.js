import React from 'react';
import { TextInput, View, Text } from 'react-native';
import {APP_COLORS} from '../constants/Colors';

const InputSansLabel = ({ active, onFocus, editable, value, onChangeText, placeholder, secureTextEntry, onEndEditing, symboleMonnaie }) => {
	const { symboleContainer, inputStyle, labelStyle, containerStyle, symboleStyle } = styles;

        
	return (
		<View style={ (active) ? containerStyle : [containerStyle ,{borderWidth: 0.5}]}>
			<View style={symboleContainer} >
				<TextInput
					onFocus={onFocus}
					editable={editable}
					secureTextEntry={secureTextEntry}
					placeholder={placeholder}
					autoCorrect={false}
					style={inputStyle}
					value={value}
					onChangeText={onChangeText}
					onEndEditing= {onEndEditing}
					autoCapitalize = 'none'
					underlineColorAndroid="rgba(0,0,0,0)"   
					autoCorrect={false}
					maxLength={30}
				/> 
				<View style={symboleStyle} >
					<Text style={{fontSize: 16}}>{symboleMonnaie}</Text>
				</View>  
			</View>
		</View>);

};

const styles = {
	containerStyle: { 
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center', 
		paddingHorizontal: 8,
		borderRadius: 5,
		borderWidth: 1,
		borderColor: APP_COLORS.secondaryText  || 'gray',  
		width: '100%',
		height: 38,
		backgroundColor: APP_COLORS.primary || 'white',
	},
	inputStyle: {
		color:  APP_COLORS.primaryText || 'black',
		lineHeight: 18,
		fontSize: 16,    
		flex: 8,  
		alignContent : 'flex-start',
	},
	symboleStyle : {
		flex: 2,    
		alignItems: 'center',
	},
	symboleContainer: {
		flexDirection: 'row',
	},
};

export { InputSansLabel };