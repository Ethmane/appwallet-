import { combineReducers } from 'redux';
import { ADD_BUDGET, ADD_SPENDING, EDIT_SPENDING, REMOVE_SPENDING, ADD_CATEGORY, EDIT_CATEGORY, REMOVE_CATEGORY, EDIT_TOTAL } from '../actions/types';

const INITIAL_STATE = {
    budget: 0,
    summary: 0,
    spendings: [],
    categories: [],
    
};

const spendingsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_SPENDING:
        return { 
            ...state,
            spendings: action.payload
        };
    case EDIT_SPENDING:
        return { 
            ...state,
            spendings: action.payload
        };
    case REMOVE_SPENDING:
        return { 
            ...state,
            spendings: action.payload
        };
    default:
      return state
  }
};

const budgetReducer = (state = INITIAL_STATE, action) => {
   
    switch (action.type) {
        case ADD_BUDGET:
            return {
                ...state,
                budget: action.payload
            };
        default:
            return state
    }
};

const summaryReducer = (state = INITIAL_STATE, action) => {
   
    switch (action.type) {
        case EDIT_TOTAL:
            return {
                ...state,
                summary: action.payload
            };
        default:
            return state
    }
};

const categoriesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ADD_CATEGORY:
            return {
                ...state,
                categories: action.payload
            };
        case EDIT_CATEGORY:
            return {
                ...state,
                categories: action.payload
            };
        case REMOVE_CATEGORY:
            return {
                ...state,
                categories: action.payload
            };
        default:
            return state
    }
};

export default combineReducers({
    categoriesReducer,
    budgetReducer,
    spendingsReducer,
    summaryReducer
});

