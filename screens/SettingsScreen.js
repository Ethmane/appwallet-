import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Card, CardSection, LightButtonForm } from '../components';
import {APP_COLORS} from '../constants/Colors';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions';

class SettingsScreen extends Component {
  constructor (props) {  
    super(props)
    const { dispatch } = props
    this.boundActionCreators = bindActionCreators(Actions, dispatch)
  }

  static navigationOptions =({navigation}) => {
    return {
        title: 'RÉGLAGES',
        headerLeft: (<View></View>),
        headerRight: (<View></View>),
        headerStyle: { 
            backgroundColor: APP_COLORS.darkPrimary,  
        },
        headerTitleStyle: {
            color : APP_COLORS.primary,  
            display: 'flex',
            flex: 1,
            textAlign: 'center',
            fontWeight: 'normal',
            fontSize: 15,
        }
    }  
  }

  clearStorage = () => {
    
      const actionSummary = Actions.editSpendingsSummary(0);
      this.props.dispatch(actionSummary);
      const actionBudget = Actions.addBudget(0);
      this.props.dispatch(actionBudget);
      const actionSpendingsList = Actions.editSpending([]);
      this.props.dispatch(actionSpendingsList);
      const actionCategoryList = Actions.editCategory([]);
      this.props.dispatch(actionCategoryList);
      this.props.navigation.navigate('Budget');
   
  }

  render() {
    const { navigate } = this.props.navigation;
    const { container } = styles;
    return (
      <View style={styles.container}>
        <Card>
          <CardSection>
            <LightButtonForm 
              color={APP_COLORS.primaryAction}
              onPress={() => navigate('Budget')}
              >
              Définir budget
            </LightButtonForm>
          </CardSection>
          <CardSection>
            <LightButtonForm 
              color={APP_COLORS.primaryAction}
              onPress={() => navigate('Categories')}
              >
              Définir catégories
            </LightButtonForm>
          </CardSection>
          <CardSection last>
            <LightButtonForm 
            color={APP_COLORS.primaryAction}
            onPress={() => this.clearStorage()}
            >
              Effacer tous les résultats
            </LightButtonForm>
          </CardSection>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: APP_COLORS.lightPrimaryColor,
  }
});


export default connect()(SettingsScreen);
