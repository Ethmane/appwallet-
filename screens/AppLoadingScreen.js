import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Text
} from 'react-native';
import {APP_COLORS} from '../constants/Colors';

class AppLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstLaunch: null
    }
    this._bootstrapAsync();
  }

  // Fetch the budget from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const budget = await AsyncStorage.getItem('BugdetValue');
    this.props.navigation.navigate(budget ? 'Main' : 'Budget');
  };

  componentDidMount = () => {
    this._bootstrapAsync();
   /*AsyncStorage.getItem("alreadyLaunched")
    .then((value) => {
      if(value === null) {
        this.props.navigation.navigate('Tutorial');
        AsyncStorage.setItem('alreadyLaunched', 'true').then(() => {
          this.props.navigation.navigate('Tutorial');
        })
      } else {
        this._bootstrapAsync();
      }
    })*/
  }
  
  // Render any loading content that you like here
  render() {
    return (
      <View>
            <ActivityIndicator color={APP_COLORS.primaryAction} />
            <StatusBar barStyle="default" />
      </View>
    );
  }
}

export default AppLoadingScreen;