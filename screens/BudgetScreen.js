import React from 'react';
import {
  View, StyleSheet, AsyncStorage, Keyboard, TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import { InputForm, ErrorModal, ActionButton, Card, CardSection} from '../components';
import { APP_COLORS } from '../constants/Colors';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions';

class BudgetScreen extends React.Component {
  constructor (props) {
    super(props)
    const { dispatch } = props
    this.state = {
        inputLabel: "Votre budget",
        inputText: '',
        inputPlaceholder: '0',
        error : '',
        errorVisible: false,
    }
    this.boundActionCreators = bindActionCreators(Actions, dispatch)
  }

  static navigationOptions =({navigation}) => {
    return {
        title: 'DÉFINIR BUDGET',
        headerLeft: (<Icon name="arrow-left" size={35} style={{color: "#FFF", paddingLeft: 10}} onPress={ () => navigation.goBack() } />),
        headerRight: (<View></View>),
        headerStyle: { 
            backgroundColor: APP_COLORS.darkPrimary,  
        },
        headerTitleStyle: {
            color : APP_COLORS.primary,  
            display: 'flex',
            flex: 1,
            textAlign: 'center', 
            fontWeight: 'normal',
            fontSize: 15,
        }
    }  
    }  

    componentDidMount = async () => {
        //let {budget} = this.props;
        const budget = await AsyncStorage.getItem('BugdetValue');
        const budgetNumber = JSON.parse(budget)
        if(budget){
            this.setState({'inputText': budgetNumber});
        }         
    }

    onPressButton = () => {
        this.setBudget().done();
    }

    setBudget = async () => {
        let budget = this.state.inputText;
        if( parseInt(budget) > 0 ){
            try {
                await AsyncStorage.setItem('BugdetValue', JSON.stringify(budget));
                let action = Actions.addBudget(budget);
                this.props.dispatch(action);
                this.props.navigation.navigate('Spendings');
            } catch (error) {
                error = 'Erreur au niveau d\'ajout de budget' ;
                this.toggleError({error});
            }
            
        } else{
            error = 'Veuillez entrer toutes les informations demandées ' ;
            this.toggleError({error});
        }
    }

    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }
        
    // supprimer tous les caractères non numériques du texte d'entrée
    onChangedNumber = ({inputText}) => {
        this.setState({
            inputText: inputText.replace(/[^0-9]/g, ''),
        });
    }

  render() {
      const { inputLabel, inputText, inputPlaceholder, errorVisible, error } = this.state;
      const { buttonIcon, buttonSpacing, container, modal } = styles;
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} >
        <View style={container}>
            <Card>
                <CardSection> 
                    <InputForm 
                    active={true}
                    editable={true}  
                    label={inputLabel}
                    value={inputText}
                    onChangeText={(inputText) =>  this.onChangedNumber({inputText})}
                    placeholder={inputPlaceholder}
                    symboleMonnaie='€'
                    keyboardType={'numeric'}
                    /> 
                </CardSection>
                <CardSection  style={buttonSpacing}>    
                    <ActionButton onPress={this.onPressButton} >  
                        <IconIonicons name="ios-checkmark" size={45}  style={buttonIcon} />
                    </ActionButton>
                </CardSection>
            </Card>
            <ErrorModal visible={errorVisible} style={modal}>{error}</ErrorModal>
        </View>
    </TouchableWithoutFeedback>
    );
  }

}

const mapStateToProps = (state) => ({
    budget: state.budget
});

export default connect(mapStateToProps)(BudgetScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: APP_COLORS.lightPrimaryColor,
  },
  modal: {
    position: 'absolute',
    top: 100
  },
  buttonIcon: {
      color: APP_COLORS.primary,
  },
  buttonSpacing: {
    padding: 20, 
    alignSelf: 'flex-end'
  }
});
