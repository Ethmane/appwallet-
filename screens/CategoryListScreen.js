
import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, ScrollView } from 'react-native';
import {APP_COLORS} from '../constants/Colors';
import Icon from 'react-native-vector-icons/EvilIcons';
import { LightButtonForm, Card, CardSection, MyListItem, ButtonForm, FadingView, ErrorModal } from '../components';
import Modal from 'react-native-modal';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions';

class CategoryListScreen extends Component {     

    constructor (props) {
        super(props)
        const { dispatch } = props
        this.state = {
            error: '',
            errorVisible: false,

            data : [],
            addButtonText: `Ajouter une catégorie`,
           
            isModalVisible: false,
            modalText: `Que voudriez-vous faire ...`,
            buttonModifyCategory: `Modifier`,
            buttonRemoveCategory: `Supprimer`,
            message: '',
            selectedItem: {},

            message: 'Cette catégorie est attribué aux certain dépenses, toute les dépenses liee à cette categorie vont etre supprimé',
            textForErrorButton: `Supprimer avec dépenses`,
        }   
        this.boundActionCreators = bindActionCreators(Actions, dispatch)
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'CATÉGORIES LIST',
            headerLeft: (<Icon name="arrow-left" size={35} style={{color: "#FFF", paddingLeft: 10}} onPress={ () => navigation.navigate('Settings') } />),
            headerRight: (<View></View>),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,  
            },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                display: 'flex',
                flex: 1,
                textAlign: 'center',
                fontWeight: 'normal',
                fontSize: 15,
            }
        }  
    }  

    removeCategory = () => {
        const { id } = this.state.selectedItem;
        const { categories, spendings } = this.props;
        const categoryHasSpendings = spendings.some((el) => { return el.category.id === id } );
       
        if(!categoryHasSpendings){
            const newCategoryList = categories.filter(item => item.id !== id)
            this.setState({ data : Array.from(newCategoryList) });
            const action = Actions.removeCategory(newCategoryList);
            this.props.dispatch(action);
            this.toggleModal();
        } else {
            this.toggleModal();
            setTimeout(() => {
                this.setState({isAlertModalVisible: true});
            }, 700);  
        }
    }
  
    removeCategoryWithExpenses = () => {
        const { categories, spendings, summary } = this.props;
        const { selectedItem } = this.state;
        const newCategoryList = categories.filter(item => item.id !== selectedItem.id)
        this.setState({ data : Array.from(newCategoryList) });
        const action = Actions.removeCategory(newCategoryList);
        this.props.dispatch(action);
        const newSpendingsList = spendings.filter(item => item.category.id !== selectedItem.id)
        let actionSpending = Actions.removeSpending(newSpendingsList);
        this.props.dispatch(actionSpending);

        const newSummary = Number(summary) - Number(selectedItem.value);
        let actionSummary = Actions.editSpendingsSummary(newSummary);
        this.props.dispatch(actionSummary);

        this.toggleModalAlert();
    }

    goToAddCategory = () => {
        this.props.navigation.navigate('AddCategory', {
            goBack: 'Categories',
        });
    }

    toggleModal = () => {
       this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    toggleModalAlert = () => {
        this.setState({ isAlertModalVisible: !this.state.isAlertModalVisible })
     }

    onPressItem = (item) => {
        this.toggleModal();
        this.setState({ selectedItem:  item });
    };

    goToModifyCategory = () => {
        let { selectedItem } = this.state;
        this.toggleModal();
        setTimeout(() => {
            this.props.navigation.navigate('AddCategory', {'category': selectedItem, 'goBack': 'Categories' })
        }, 700);  
    }

    onRemoveCategoryClick = () => {
        this.removeCategory();
    }

    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }

    render() {
        const { errorVisible, error, isModalVisible, modalText, buttonModifyCategory, buttonRemoveCategory, addButtonText, message, isAlertModalVisible} = this.state;
        const { categories } = this.props;
        const { container, modalCloseButton, icon, modal, alert } = styles;

        return (
            <ScrollView contentContainerStyle={container}>
                    <Card >  
                        <FlatList
                        data={categories}
                        extraData={this.state}
                        keyExtractor={item => item.id}
                        renderItem={ ({item}) => 
                            <CardSection>
                                <MyListItem
                                id={item}
                                onPressItem={this.onPressItem}
                                title={item.name}
                                color={item.color}
                                />    
                            </CardSection>
                        }
                        />
                        <CardSection last>
                            <LightButtonForm
                            color={APP_COLORS.primaryAction}
                            onPress={this.goToAddCategory}
                            >
                            {addButtonText}
                            </LightButtonForm>
                        </CardSection>
                    </Card> 
                    <ErrorModal visible={errorVisible} style={modal}>{error}</ErrorModal>
                    <Modal isVisible={isModalVisible}>
                        <Card>
                            <TouchableOpacity onPress={this.toggleModal} style={modalCloseButton} >
                                <Icon name="close" size={25} style={icon} />
                            </TouchableOpacity>                          
                            <CardSection>
                                <Text style={modalText}>{modalText}</Text>
                            </CardSection>
                         
                            <CardSection last>
                                <ButtonForm onPress={this.goToModifyCategory}>{buttonModifyCategory}</ButtonForm>
                                <View style={{width: 10}}></View>
                                <ButtonForm onPress={this.onRemoveCategoryClick}>{buttonRemoveCategory}</ButtonForm>
                            </CardSection>  
                        </Card>
                    </Modal> 
                    <Modal isVisible={isAlertModalVisible}>
                        <Card>
                            <TouchableOpacity onPress={this.toggleModalAlert} style={modalCloseButton} >
                                <Icon name="close" size={25} style={icon} />
                            </TouchableOpacity>                          
                            <CardSection>
                                <Text style={alert}>{message}</Text>
                            </CardSection>

                            <CardSection last>
                                <ButtonForm onPress={this.toggleModalAlert}>Annuler</ButtonForm>
                                <View style={{width: 10}}></View>
                                <ButtonForm onPress={this.removeCategoryWithExpenses}>{buttonRemoveCategory}</ButtonForm>
                            </CardSection>  
                        </Card>
                    </Modal>                                       
            </ScrollView>
        );
    }

}

const mapStateToProps = (state) => ({
    categories: state.categoriesReducer.categories,
    spendings: state.spendingsReducer.spendings,
});

export default connect(mapStateToProps)(CategoryListScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: APP_COLORS.lightPrimaryColor,
    },
    button: {
        flex: 1,
        paddingRight: 0,
        marginRight: 0,
    },
    actionButtonIcon: {
        fontSize: 40,
        height: 40,
        color: APP_COLORS.primary,
      },
    showlist : {
        paddingHorizontal: 10,
        paddingTop: 10,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: APP_COLORS.lines || 'gray',
        position: 'relative',
    },
    modalTextStyle: {
        fontSize: 18,
        paddingHorizontal: 10,
        paddingVertical: 20,
        color: APP_COLORS.secondaryText,
    },
    errorContainer: {
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        width: 300, 
        marginTop: 40,
        alignSelf: 'center',  
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5,
    },
    errorText: {
        fontSize: 14,
        color: APP_COLORS.primary || 'white',
        lineHeight: 22,
    },  
    modalCloseButton: {
        alignItems: 'flex-end', 
        paddingTop: 10, 
        paddingRight: 10
    }, 
    icon: {
        color: APP_COLORS.secondaryText
    },
    modal: {
        position: 'absolute',
        top: 100,
    },
    alert: {
        color: APP_COLORS.primaryAction
    }
});




