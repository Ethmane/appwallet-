import React, { Component } from 'react';
import { View, StyleSheet, Keyboard, TouchableWithoutFeedback } from 'react-native';
import {APP_COLORS} from '../constants/Colors';
import Icon from 'react-native-vector-icons/EvilIcons';
import { InputSansLabel, ButtonForm, Card, CardSection, ErrorModal } from '../components';
import { ColorPicker,  fromHsv  } from 'react-native-color-picker';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions';

class AddCategoryScreen extends Component {


    constructor (props) {
        super(props)
        const { dispatch } = props
        this.state = {

            inputLabel: '',
            inputText: '',
            inputPlaceholder: "Nom de catégorie",
  
            color: '#ff0022',
            buttonTitle: "Valider",

            error : '',
            errorVisible: false,
            
            categoryToModify: {},
            categoryList: [],
        }    
        this.boundActionCreators = bindActionCreators(Actions, dispatch) 
    }

    static navigationOptions = ({navigation}) => {
        //console.log(JSON.stringify(navigation.state.params)); 
        const headerTitle = (navigation.state.params.goBack && navigation.state.params.goBack ==="NewExpense") ? 'NOUVELLE CATÉGORIE': 'MODIFIER CATÉGORIE';
        return {
            title:  headerTitle,
            headerLeft: (<Icon name="arrow-left" size={35} style={{color: "#FFF", paddingLeft: 10}} onPress={ () => navigation.goBack() } />),
            headerRight: (<View></View>),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,
            },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                display: 'flex',
                flex: 1,
                textAlign: 'center', 
                fontWeight: 'normal',
                fontSize: 15,
            }
      }  
    }  

    componentDidMount (){
        let {state} = this.props.navigation;
        if(state && state.params && state.params.category){
            const { name, color } = state.params.category;
            this.setState({
                inputText: name, 
                color: color
            });
           
        }
    }

    modifyCategory = () => {
        const { categories, spendings } = this.props;
        const { color, inputText } = this.state;
        let { category } = this.props.navigation.state.params;
        let error;
        
        if( inputText.length > 1 && color.length > 1 ) {
            let newCategoryList = categories.map((el) => {
                if(el.id == category.id){
                    el.color = color;
                    el.name = inputText;
                }
                return el;
            });
            let newSpendingsList = spendings.map((el) => {
                if(el.category.id == category.id){
                    el.category.color = color;
                    el.category.name = inputText;
                }
                return el;
            })
            let action = Actions.editCategory(newCategoryList);
            this.props.dispatch(action);
            let actionSpendings = Actions.editSpending(newSpendingsList);
            this.props.dispatch(actionSpendings);
            this.returnToListOfCategories(null);
        } else if (!noCategoryWithTheSameName) {
            error = 'Nom de catégorie déjà existant ' ;
            this.toggleError({error}); 
        }  else {
            error = 'Veuillez entrer toutes les informations demandées' ;
            this.toggleError({error});
        }
    }

    addCategory = () => {
        const { categories } = this.props;
        const { color, inputText } = this.state;
        let newCategory = {
            id: Date.now().toString(),
            color:  color,
            name:   inputText,
            spendings: 0,
        };
       
        let error; 
        
        const noCategoryWithTheSameName =  (categories.length === 0 ) || categories.every((el) => {return el.name !== inputText});
        
        if( noCategoryWithTheSameName && newCategory.name.length > 1 && newCategory.color.length > 1 ){
            let newCategoryList;
            if(categories){
                newCategoryList = [... categories, newCategory];   
            } else {
                newCategoryList = [newCategory];
            }
           
            let action = Actions.addCategory(newCategoryList);
            this.props.dispatch(action);
            
            this.returnToListOfCategories(newCategory)
        } else if ( newCategory.name.trim().length < 1 && newCategory.color.length < 1 ) {
            error = 'Veuillez entrer toutes les informations demandées ' ;
            this.toggleError({error});
        } else if ( newCategory.name.trim().length < 1 && newCategory.color.length > 1 ) {
            error = 'Nom de catégorie incorrect ou trop court ' ;
            this.toggleError({error});  
            
        } else if (!noCategoryWithTheSameName) {
            error = 'Nom de catégorie déjà existant ' ;
            this.toggleError({error}); 
            
        }  
    }

    onAcceptButtonPress = () => {
        Keyboard.dismiss();  
        let {state} = this.props.navigation;
        if(state.params && state.params.category){
            this.modifyCategory(); 
        } else {
            this.addCategory();
        }   
    }

    returnToListOfCategories = (newCategory) => {
        let {inputValueText,inputDescText, goBack} = this.props.navigation.state.params;

        if(goBack === 'NewExpense') {
            this.props.navigation.push(goBack, {
                category: newCategory,
                inputValueText,
                inputDescText
            });
        } else {
            this.props.navigation.push(goBack,{
                inputValueText,
                inputDescText
            });
        }    
    } 

    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);

    }

    setColor = (color) => {
        if(typeof color === "string") {
            this.setState({color});
        } else {
            this.setState({color: fromHsv(color)});
        } 
    }

    render() {
        const { errorVisible, error, inputPlaceholder, inputText, buttonTitle } = this.state;
        const { container, modal } = styles;
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View style={container}>
                    <Card style={{flex: 7}}>
                        <CardSection style={{flex: 1}}>
                            <InputSansLabel 
                            active={true}
                            editable={true}
                            placeholder={inputPlaceholder}
                            value={inputText}
                            onChangeText={(inputText) => {this.setState({inputText})}}
                            /> 
                        </CardSection>
                        <CardSection style={{flex: 5}}>
                            <ColorPicker
                            onColorSelected={this.setColor}
                            onColorChange={this.setColor}
                            style={{flex: 1}}
                            />
                        </CardSection>
                        <CardSection style={{flex: 1, alignItems: 'flex-end'}} last>
                            <ButtonForm
                            onPress = {this.onAcceptButtonPress} 
                            >{buttonTitle}</ButtonForm>
                        </CardSection>
                    </Card>
                    <ErrorModal visible={errorVisible} style={modal}>{error}</ErrorModal>
                </View>
                
            </TouchableWithoutFeedback> 
        );
    }

}

const mapStateToProps = (state) => ({
    categories: state.categoriesReducer.categories,
    spendings: state.spendingsReducer.spendings,
});

export default connect(mapStateToProps)(AddCategoryScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,   
        justifyContent: 'space-between',
        paddingVertical: 20,
        backgroundColor: APP_COLORS.lightPrimaryColor,
    },
    button: {
        flex: 1,  
        paddingRight: 0,
        marginRight: 0,
    },
    actionButtonIcon: {
        fontSize: 40,
        height: 40,
        color: APP_COLORS.primary,
      },
    showlist : {
        paddingHorizontal: 10,
        paddingTop: 10,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: APP_COLORS.lines || 'gray',
        position: 'relative',
    },
    modal: {
        position: 'absolute',
        top: 70
    },
});

