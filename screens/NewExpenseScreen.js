import React, { Component } from 'react';
import { AppRegistry, View, Text, StyleSheet, Button as ButtonRN,  TouchableHighlight, AsyncStorage, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Container, Header, Title, Content, Button, Icon as IconeNB, Right, Body, Left, Picker, Form,  H3, Item as FormItem } from 'native-base';
const Item = Picker.Item;
import Icon from 'react-native-vector-icons/EvilIcons';
import {APP_COLORS} from '../constants/Colors';
import { InputForm, ButtonForm, CardSection, Card, ErrorModal } from '../components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions';

class NewExpenseScreen extends Component {

    constructor (props) {
        super(props)
        const { dispatch } = props
        this.state = {  

            inputValueLabel: "Montant depensé",
            inputValueText: '',
            inputValuePlaceholder: '0',

            inputDescLabel: "Description",
            inputDescText: '',      
            inputDescPlaceholder: 'lait',
            
            inputCategoryLabel: "Catégorie",
            choosenCategory: null,

            errorVisible: false,
            error: '',
        }
        this.boundActionCreators = bindActionCreators(Actions, dispatch)
    }  

    static navigationOptions = ({navigation}) => {
        const headerTitle = (navigation.state.params) ? 'MODIFIER CETTE DÉPENSE' : 'NOUVELLE DÉPENSE';
        return {
            title: headerTitle,
            headerLeft: (<Icon name="arrow-left" size={35} style={{color: "#FFF", paddingLeft: 10}} onPress={ () => navigation.push('Spendings') } />),
            headerRight: (<View></View>),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,  
            },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                display: 'flex',
                flex: 1,
                textAlign: 'center',
                fontWeight: 'normal',
                fontSize: 15,
            }
        }  
    }

    componentDidMount (){
        let {state} = this.props.navigation;
        if(state && state.params && state.params.spending){
            const { value, description, category } = state.params.spending;
            this.setState({
                inputValueText: value, 
                inputDescText: description,
                choosenCategory: category,
                oldValue: value, 
            });
        }
        if(state && state.params && state.params.category){
            let {inputValueText,inputDescText} = state.params;

            this.setState({
                choosenCategory: state.params.category,
                inputValueText,
                inputDescText
            });
        }
    }

    modifyExpense = (item) => {
        const { spendings , summary, budget } = this.props;
        const { inputValueText, inputDescText, choosenCategory, oldValue} = this.state;
        const expense = {
            id: item.id,
            time: item.time,
            value: inputValueText,
            description: inputDescText,
            category: choosenCategory,
        };
        let error;
        if( expense.value.length > 0 && expense.description.length > 1  && expense.category != null ) {
          
                const newListOfSpendings = spendings.map((el) => {
                    if(el.id == item.id){
                        el = expense;
                    }
                    return el;
                });
                
                const newSummary = Number(summary) + Number(expense.value) - Number(oldValue);
                if(newSummary < budget){
                    const actionSummary = Actions.editSpendingsSummary(newSummary);
                    const actionSpendings = Actions.addSpending(newListOfSpendings);
                    this.props.dispatch(actionSpendings);
                    this.props.dispatch(actionSummary);
                    this.returnToListOfDepenses();
                } else {
                    error = 'Cette dépense dépasse votre budget, veuillez entrer une valeur inferieur';
                    this.toggleError({error});
                }
             
        } else {
            error = 'Veuillez entrer toutes les informations demandées' ;
            this.toggleError({error});
        }
    }
    
    //afficher les options du sélecteur
    renderPicker = () => {
        let { categories } = this.props;
        return (
            
            categories.map( (el, i) => {
            return (  <Item key={i} label={el.name} value={el} />);
            })
            
        );
    }

    //changer de catégorie
    onCategoryChoice = (value) => {
        this.setState({
            choosenCategory: value,
        });
    }

    //aller à l'écran d'ajout de catégorie
    goToAddCategory = () => {
        let {inputValueText ,inputDescText} = this.state;
  
        this.props.navigation.navigate('AddCategory', {
            goBack: 'NewExpense',
            inputValueText,
            inputDescText
        });
        
    }

    //montrer le sélecteur dans le nouvel écran
    showCategories = () => { 
        const { choosenCategory } = this.state;
        return  (
            <Picker
                renderHeader={backAction =>
                    <Header style={{ backgroundColor: APP_COLORS.darkPrimary, height: 90, paddingTop: 35 }}>
                        <Left style={{ flex: 1}}>
                            <Button transparent onPress={backAction}>
                            <Icon name="arrow-left" size={35} style={{ color: APP_COLORS.primary, paddingLeft: 5 }} />
                            </Button>
                        </Left>
                    </Header>}
                mode="dropdown"  
                placeholder={choosenCategory ? choosenCategory.name : "Choisir"}
                iosHeader="CHOISIR"
                selectedValue={choosenCategory}
                onValueChange={ (value) => this.onCategoryChoice(value)}
            >
            <Item label='choisir' value='' />
            {this.renderPicker()}
            </Picker>
      )
    }

    //exécuter après avoir cliqué sur le bouton accepter
    onAcceptPress = () => {
        let {state} = this.props.navigation;
        const { navigation } = this.props;
        const itemId = navigation.getParam('mode', '');
        console.log('**************<=======> 1', JSON.stringify(this.props.navigation));
        console.log('**************<=======> 2 ', itemId);
        console.log('**************<=======> 3 ', !!state.params);
       // if(state.params && state.params.spending !== undefined){
        if(state.params && itemId != ''){
            console.log('**************<=======>************ modifier ')
            this.modifyExpense(state.params.spending);
        } else {
            console.log('**************<=======>************ save ',)
            this.saveDepense();
        }   
    }

    //sauver nouveau depense aux mémoires de téléphone
    saveDepense = () => { 
        
        let { spendings , summary, budget } = this.props;
        let { inputValueText, inputDescText, choosenCategory } = this.state;
        let expense = {
            id: Date.now().toString(),
            time: new Date(),
            value: inputValueText,
            description: inputDescText,
            category: choosenCategory,
        };
        let error;
        if( expense.value.length > 0 && expense.description.length > 1  && expense.category != null ) {
            if(spendings){
                newListOfSpendings = [ ...spendings, expense];
            } else {
                newListOfSpendings = [expense];
            }
            //console.log(newListOfSpendings, choosenCategory)
            const newSummary = Number(summary) + Number(expense.value);
            if(newSummary < budget){
                const actionSummary = Actions.editSpendingsSummary(newSummary);
                const actionSpendings = Actions.addSpending(newListOfSpendings);
                this.props.dispatch(actionSpendings);
                this.props.dispatch(actionSummary);
                this.returnToListOfDepenses(); 
            } else {
                error = 'Cette dépense dépasse votre budget, veuillez entrer une valeur inferieur';
                this.toggleError({error});
            }
        } else{     
            error = 'Veuillez entrer toutes les informations demandées, la description doit contenir au moins deux lettres';
            this.toggleError({error});
        }
    }

    //naviguer vers l'écran 'ListOfDepenses'
    returnToListOfDepenses = () => {
        this.props.navigation.push('Spendings');
    }

    // Afficher le message d'erreur et disparaître après 3 secondes
    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }

    // supprimer tous les caractères non numériques du texte d'entrée
    onChangedNumber = ({inputValueText}) => {
        this.setState({
            inputValueText: inputValueText.replace(/[^0-9]/g, ''),
        });
    }
    
    render() {
        const {
            inputValueLabel, inputValueText, inputValuePlaceholder, inputDescLabel, inputDescText, inputDescPlaceholder,
            errorVisible, error
        } = this.state;
        const { container, contentPickerStyle, labelPickerStyle, PickerStyle, modal, spaceBetweenButtons, iconStyle } = styles;
        
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View style={container}>
                    <Card >
                        <CardSection> 
                            <InputForm 
                            active={true}
                            type="only-numbers"
                            editable={true}  
                            label={inputValueLabel}
                            value={inputValueText}
                            onChangeText={(inputValueText) =>  this.onChangedNumber({inputValueText})}
                            placeholder={inputValuePlaceholder}
                            symboleMonnaie='€'
                            keyboardType={'numeric'}
                            /> 
                        </CardSection>
                        <CardSection> 
                            <InputForm 
                            active={true}
                            editable={true}
                            label={inputDescLabel}
                            value={inputDescText}
                            onChangeText={(inputDescText) => {this.setState({inputDescText})}}
                            placeholder={inputDescPlaceholder}
                            /> 
                        </CardSection>
                        <CardSection> 
                            <View style={contentPickerStyle}>
                                <Text style={labelPickerStyle}> Catégorie </Text> 
                                <View style={PickerStyle}> 
                                    {this.showCategories()}
                                    <Icon 
                                    name="plus" size={20} style={iconStyle}
                                    onPress={this.goToAddCategory}
                                    />
                                </View>
                            </View>                         
                        </CardSection>
                        <CardSection last>   
                            <ButtonForm  
                            onPress={this.returnToListOfDepenses}
                            >
                            Annuler
                            </ButtonForm>                           
                            <View style={spaceBetweenButtons}></View>                        
                            <ButtonForm   
                            onPress={this.onAcceptPress}  
                            >
                            Valider
                            </ButtonForm>     
                        </CardSection>
                    </Card>
                    <ErrorModal visible={errorVisible} style={modal}>{error}</ErrorModal>   
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const mapStateToProps = (state) => ({
    categories: state.categoriesReducer.categories,
    spendings: state.spendingsReducer.spendings,
    summary: state.summaryReducer.summary,
    budget: state.budgetReducer.budget,
});

export default connect(mapStateToProps)(NewExpenseScreen);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: APP_COLORS.lightPrimaryColor,
    },
    input: {  
        flex: 1,
        paddingHorizontal: 20,
    },
    button: {
        flex: 1,
        paddingRight: 0,
        marginRight: 0,
    },
    actionButtonIcon: {
        fontSize: 35,
        height: 35,
        color: APP_COLORS.primary,
    },
    buttonText: {
        fontSize: 15,
        lineHeight: 15,
        color: APP_COLORS.primary,
    },
    contentPickerStyle: { 
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center', 
        paddingHorizontal: 8,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: APP_COLORS.secondaryText  || 'gray',  
        height: 40,
        width: '100%',
        backgroundColor: APP_COLORS.primary || 'white',
    },
    labelPickerStyle: {   
        fontSize: 16,
        flex: 8, 
    },
    PickerStyle: { 
        flex: 6,  
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignContent : 'flex-start',
    },
    modal: {
        position: 'absolute',
        top: 70,
    },
    spaceBetweenButtons: {
        width: 10
    },
    iconStyle: {
        color: APP_COLORS.darkPrimary, 
        paddingLeft: 5
    }
});



