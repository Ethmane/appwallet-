import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, AsyncStorage, FlatList, TouchableOpacity, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Button, ListItem, Card, CardSection, ButtonForm, FadingView, ErrorModal, ActionButton } from '../components'; 
import { APP_COLORS } from '../constants/Colors';
import Icon from 'react-native-vector-icons/EvilIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions';
const SCREEN_WIDTH = Dimensions.get('window').width;

class SpendingsScreen extends Component {  
    constructor (props) {  
        super(props)
        const { dispatch } = props
        this.state = {
            bottomBarTitleLeft: "Votre budget",
            bottomBarTitleRight: "Vos dépenses",
            monnais: '€',

            error: '',
            errorVisible: false,

            selected: '',

            isModalVisible: false,
            selectedItem: null,
        }
        this.boundActionCreators = bindActionCreators(Actions, dispatch)
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'LISTE DES DÉPENSES',
            headerLeft: (<View></View>),
            headerRight: (<View></View>),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,  
            },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                display: 'flex',
                flex: 1,
                textAlign: 'center',
                fontWeight: 'normal',
                fontSize: 15,
            }
        }  
    }  
  
    removeDepense = () => {

        const { selectedItem } = this.state;
        const {spendings, summary } = this.props;
        const newSummary = Number(summary) - Number(selectedItem.value);
        const newSpendingsList = spendings.filter(item => item.id !== selectedItem.id)
        let actionSummary = Actions.editSpendingsSummary(newSummary);
        this.props.dispatch(actionSummary);
        let action = Actions.removeSpending(newSpendingsList);
        this.props.dispatch(action);
    }
  
    onRemoveDepenseClick = () => {
        this.removeDepense();
        this.toggleModal();
    }

    renderBadge = (item) => {
        let color = item.category.color; 
        return ( 
            <View style={[styles.circle, {backgroundColor: color}]} />
        );
    }

    onPressAddButton = () => {
        this.props.navigation.push('NewExpense');
    }

    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    goToModifyDepense = () => {
        this.toggleModal();
        const { selectedItem } = this.state;
        const { spendings } = this.props;
        let item = spendings.find((el) => {if(el.id === selectedItem.id){return el}});
        let {navigation} = this.props;
        setTimeout(() => {
            //this.props.navigation.push('NewExpense', {'spending': item });
           navigation.navigate('NewExpense', {
               spending: item,
                mode : 'M'
            }); 
        }, 700) 
    }

    onPressItem = (item) => {  
        this.toggleModal();
        this.setState({ selectedItem:  item });
    };

    render() {
    
    const { container, itemTime, itemText, itemIcone, itemDescription, itemCategory, itemValeur, 
         bottomBar, barDescription, barValue, modalText } = styles;
    const { errorVisible, error, monnais, bottomBarTitleLeft, bottomBarTitleRight, 
        isModalVisible } = this.state;
    const { budget, spendings, summary} = this.props;
    //console.log('spendings', spendings)
    return (
            <View style={container}>
                
                <View style={{flex: 10}}>
                    <ErrorModal visible={errorVisible}>{error}</ErrorModal>  
                    
                    {spendings.length > 0 ? (<FlatList
                        data={spendings}
                        extraData={this.state}
                        keyExtractor={item => item.id}
                        renderItem={({item}) => 
                            <ListItem   
                            item={item}
                            onPressItem={this.onPressItem}
                            >
                                <View style={itemIcone}>
                                {this.renderBadge(item)}
                                </View>  
                                <View style={itemText}>
                                    <View >
                                        <View style={{flexDirection: 'row'}}>
                                            <Text style={itemCategory}>{item.category.name}</Text>
                                            <Text style={itemDescription}>{item.description}</Text>
                                        </View>
                                        <Text style={itemTime}>{moment(item.time).format('LL')}</Text>
                                    </View>
                                    <Text style={itemValeur}>{item.value} {monnais}</Text>  
                                </View>
                            </ListItem>
                        }
                    />) : (null)}       
                </View>
                
                <View style={bottomBar}>  
                    <View>
                        <Text style={barDescription}>{bottomBarTitleLeft}</Text>
                        <Text style={barValue}>{budget} {monnais}</Text>
                    </View>
                    
                    <View style={{alignItems: 'flex-end'}}>
                        <Text style={barDescription}>{bottomBarTitleRight}</Text>
                        <Text style={barValue}>{summary} {monnais}</Text>
                    </View>
                </View>
                <View style={styles.actionButtonContainer2}>
                    <ActionButton onPress={this.onPressAddButton} >
                        <IconIonicons name="ios-add" size={45}  color={APP_COLORS.primary} />
                    </ActionButton>
                </View>
                <Modal isVisible={isModalVisible} animationOutTiming={700}>
                    <Card>
                        <TouchableOpacity onPress={this.toggleModal} style={{ alignItems: 'flex-end', paddingTop: 10, paddingRight: 10 }} >
                            <Icon name="close" size={25} style={{color: APP_COLORS.secondaryText}} />
                        </TouchableOpacity>
                        <CardSection>
                            <Text style={modalText}>Que voudriez-vous faire ...</Text>
                        </CardSection>
                        <CardSection last={true}>
                            <ButtonForm onPress={ this.goToModifyDepense }>Modifier</ButtonForm>
                            <View style={{width: 10}}></View>
                            <ButtonForm onPress={this.onRemoveDepenseClick}>Supprimer</ButtonForm>
                        </CardSection>  
                    </Card>
                </Modal>
                
            </View>     
        );
    }
}

const mapStateToProps = (state) => ({
    budget: state.budgetReducer.budget,
    spendings: state.spendingsReducer.spendings,
    summary: state.summaryReducer.summary
});

export default connect(mapStateToProps)(SpendingsScreen);

const styles = StyleSheet.create({
  container: {
      flex: 1, 
      backgroundColor: APP_COLORS.lightPrimaryColor,
  },
  itemIcone: {
     flex: 1, 
     justifyContent: 'center',
  },
  circle: {
      borderRadius: 8,
      width: 16,
      height: 16,  
  },
  itemText: {
     flex: 10,
     flexDirection: 'row',
     alignItems: 'center', 
     justifyContent: 'space-between'
  },
  itemDescription: {
      paddingLeft: 6,
      lineHeight: 20,
      fontSize: 15,
      color: APP_COLORS.secondaryText,
  },
  itemTime:  {
      paddingLeft: 2,
      lineHeight: 20,
      fontSize: 15,
      color: APP_COLORS.secondaryText,
  },
  itemCategory: {
      lineHeight: 20,
      fontSize: 18,
      color: APP_COLORS.primaryAction,
      fontWeight: '600', 
  },
  itemValeur: {
      fontSize: 18,
      color: APP_COLORS.primaryAction,
      fontWeight: '600',
  },
  bottomBar: {
      flex: 1, 
      backgroundColor: APP_COLORS.darkPrimary,
      alignItems: 'center', 
      justifyContent: 'space-between',
      flexDirection: 'row',
      paddingHorizontal: 20,
      height: 60,
  },
  actionButtonContainer: {
      position: 'absolute',
      left: '50%', 
      right: 0,
      bottom: 30,
      marginLeft: 0, 
      marginRight: 0,
      width: SCREEN_WIDTH,
  },
  actionButtonContainer2: {
      position: 'absolute',
      bottom: 15,    
      alignSelf: 'center',
  },
  actionButtonIcon: {
      height: 35,  
      color: APP_COLORS.primary,
  },
  barDescription: {
      fontSize: 16,
      color: APP_COLORS.primary,
      fontWeight: '400',
      lineHeight: 24,
  },
  barValue: {
      fontSize: 18,
      color: APP_COLORS.primary,
      fontWeight: '600',
  },
  modalText: {
      fontSize: 18,
      paddingHorizontal: 10,
      paddingVertical: 20,
      color: APP_COLORS.secondaryText,
  }
});


