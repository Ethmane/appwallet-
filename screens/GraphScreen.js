import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {APP_COLORS} from '../constants/Colors';
import Pie from 'react-native-pie';
import { connect } from 'react-redux';

class GraphScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: '',
      errorVisible: false,

      listOfDepenses: [],
      categoryList: [],
      budgetLeft: 0,
      bugdetValue: 0,

      data: [],
      colors: [],
      chartData: []
    }
  }

  static navigationOptions =({navigation}) => {
    return {
        title: 'RÉSUMÉ',
        headerStyle: { 
            backgroundColor: APP_COLORS.darkPrimary,  
        },
        headerLeft: (<View></View>),
        headerRight: (<View></View>),
        headerTitleStyle: {
          color : APP_COLORS.primary, 
          display: 'flex',
          flex: 1,
          textAlign: 'center',
          fontWeight: 'normal',
          fontSize: 15,
        }
    }  
  }  
  componentWillMount = () => {   
    this.convertToPieChartData(this.props.spendings, this.props.categories, this.props.budget);
  }

  componentWillReceiveProps(nextProps) {
    this.convertToPieChartData(nextProps.spendings, nextProps.categories, nextProps.budget);
  }

  toggleError = ({error}) => {
    this.setState({ error, errorVisible: !this.state.errorVisible });
    setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
  }

  convertToPieChartData = (spendings, categories, budget) => {
    if(spendings && categories && budget) {
      let newCategories = categories;
      let newSpendings = spendings;

      /*let chartData = newCategories.map((el) => {
        newSpendings.map((element) => {
          if(element.category.id == el.id){
            el['spendings'] = Number(el.spendings) + Number(element.value);
          }
        })
        return el; 
      });*/

      let chartData = [];
      for(let i=0; i<newCategories.length; i++){
        for(let j=0; j<newSpendings.length; j++){
          if(newCategories[i].id === newSpendings[j].category.id){
            chartData.push({
              spendings : parseInt(newCategories[i].spendings) + parseInt(newSpendings[j].value),
              id : newCategories[i].id,
              color : newCategories[i].color,
              name : newCategories[i].name
            })    
          }
        }
      }
     
      let data = chartData.map((el) => {
        let  percentage = el.spendings/budget*100;
        return percentage;
      });
      
      let colors = chartData.map((el) => {
        return el.color;
      });
      this.setState({ data, colors, chartData});
    }
  }

  renderList = () => {
    return this.state.chartData.map( (el, i) => {
      return (  
        <View key={i} style={styles.descriptionItem}>
          <View style={[styles.circle, {backgroundColor: el.color}]} />
          <Text style={styles.descriptionText} >{el.name} -  {el.spendings} €</Text>
        </View>
      );  
    });
  }

  render() {
    const { data, colors } = this.state;
    return (
          <View style={styles.container}>
                {(data.length > 0 && colors.length > 0) ? (
                  <View>
                    <Pie
                    radius={170}
                    innerRadius={130}
                    series={data}
                    colors={colors}
                    backgroundColor='#ddd' />
                    <View style={styles.description} >
                      <Text>ENSEMBLE DES DÉPENSES:</Text>
                      {this.renderList()}
                    </View>
                  </View>
                ) : (
                  <View>
                    <Text>Veuillez entrer les données pour afficher le graph</Text>
                  </View>
                )}
          </View>
    );
  }
}

const mapStateToProps = (state) => ({
    categories: state.categoriesReducer.categories,
    budget: state.budgetReducer.budget,
    spendings: state.spendingsReducer.spendings,
    summary: state.summaryReducer.summary
});

export default connect(mapStateToProps)(GraphScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: APP_COLORS.lightPrimaryColor,
  },
  card: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  description: {
    display: 'flex',
    flexDirection: 'column',
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingLeft: 40,
    paddingTop: 50,
  },
  descriptionItem: {
    display: 'flex',  
    flexDirection: 'row',
    paddingTop: 14,
  },
  descriptionText: {
    fontSize: 14,
    paddingLeft: 10,
  },
  circle: {
    borderRadius: 8,
    width: 16,
    height: 16,  
  },
});



