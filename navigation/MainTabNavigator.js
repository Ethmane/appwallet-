import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createSwitchNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import TabBarLabel from '../components/TabBarLabel';
import GraphScreen from '../screens/GraphScreen';

import SpendingsScreen from '../screens/SpendingsScreen';
import NewExpenseScreen from '../screens/NewExpenseScreen';

import SettingsScreen from '../screens/SettingsScreen';
import BudgetScreen from '../screens/BudgetScreen';
import CategoryListScreen from '../screens/CategoryListScreen';
import AddCategoryScreen from '../screens/AddCategoryScreen';

const GraphStack = createStackNavigator({
  Graph: GraphScreen,
});

GraphStack.navigationOptions = {
  tabBarLabel: ({ focused }) => (
    <TabBarLabel
      focused={focused}
      name={'Graph'}
    />
  ),
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? 'ios-pie'
          : 'md-pie'
      }
    />
  ),
};

const SpendingsStack = createStackNavigator({
  Spendings: SpendingsScreen,
  NewExpense: NewExpenseScreen,
  AddCategory: AddCategoryScreen
});

SpendingsStack.navigationOptions = {
  
    tabBarLabel: ({ focused }) => (
    <TabBarLabel
      focused={focused}
      name={'Dépenses'}
    />
    ),
    tabBarIcon: ({ focused }) => (
      <TabBarIcon
        focused={focused}
        name={Platform.OS === 'ios' ? 'ios-list' : 'md-list'}
      />
    ),
  
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
  Budget: BudgetScreen, 
  Categories: CategoryListScreen,
  AddCategory: AddCategoryScreen
});

SettingsStack.navigationOptions = {

  tabBarLabel: ({ focused }) => (
    <TabBarLabel
      focused={focused}
      name={'Réglages'}
    />
  ),
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-cog' : 'md-cog'}
    />
  ),

};


export default createBottomTabNavigator({
    GraphStack,
    SpendingsStack,
    SettingsStack,
  }, {
    tabBarOptions : {
      style: {
        height: 70,
        paddingBottom: 10
      }
  }
}, {

  tabBarComponent: props => {
    const {navigation, navigationState} = props
    const jumpToIndex = index => {
      const lastPosition = props.getLastPosition()
      const tab = navigationState.routes[index]
      const tabRoute = tab.routeName
      const firstTab = tab.routes[0].routeName

      navigation.dispatch(pushNavigation(tabRoute))
      lastPosition === index && navigation.dispatch(resetNavigation(firstTab))
    }
    return <TabView.TabBarBottom {...props} jumpToIndex={jumpToIndex}/>
  }
 });
