import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import MainTabNavigator from './MainTabNavigator';
import AppLoadingScreen from '../screens/AppLoadingScreen';
import BudgetScreen from '../screens/BudgetScreen';
import TutorialScreen from '../screens/TutorialScreen';

export default createAppContainer(createSwitchNavigator({
    AppLoading: AppLoadingScreen,
    Main: MainTabNavigator,
    Budget: BudgetScreen, 
    Tutorial: TutorialScreen
    },
    {
    initialRouteName: 'AppLoading',
})); 

