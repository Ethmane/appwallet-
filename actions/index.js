import { ADD_BUDGET, ADD_SPENDING, EDIT_SPENDING, REMOVE_SPENDING, ADD_CATEGORY, EDIT_CATEGORY, REMOVE_CATEGORY, EDIT_TOTAL } from './types';

export const addBudget = newBudget => (
    {
      type: ADD_BUDGET,
      payload: newBudget,
    }
);

export const editSpendingsSummary = newBudget => (
  {
    type: EDIT_TOTAL,
    payload: newBudget,
  }
);

export const addSpending = spendings => (
    {
      type: ADD_SPENDING,
      payload: spendings,
    }
);

export const editSpending = spendings => (
    {
      type: EDIT_SPENDING,
      payload: spendings,
    }
);

export const removeSpending = spendings => (
  {
    type: REMOVE_SPENDING,
    payload: spendings,
  }
);

export const addCategory = categoryList => (
  {
    type: ADD_CATEGORY,
    payload: categoryList,
  }
);

export const editCategory = categoryList => (
  {
    type: EDIT_CATEGORY,
    payload: categoryList,
  }
);

export const removeCategory = categoryList => (
  {
    type: REMOVE_CATEGORY,
    payload: categoryList,
  }
);